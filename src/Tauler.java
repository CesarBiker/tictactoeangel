import java.util.Arrays;

/**
 * Created by Àngel Mariages on 11/26/15
 */

public class Tauler {
    private int amplada, alcada;
    private int fitxes[][];
    private int llocsValids[][];

    public Tauler(int amplada, int alcada) {
        this.amplada = amplada;
        this.alcada = alcada;
        fitxes = new int[amplada][alcada];
        netejarTauler();
    }

    public void netejarTauler() {
        for(int y = 0; y < fitxes.length; y++) {
            for(int x = 0; x < fitxes[y].length; x++) {
                fitxes[x][y] = 0;
            }
        }
    }

    public int canviarFitxa(int x, int y, int fitxa) {
        if(x >= 0 && x < amplada && y >= 0 && y < alcada) {
            if (fitxes[x][y] == 0) {
                fitxes[x][y] = fitxa;
                return 0;
            } else
                return 1;
        }
        return -1;
    }

    public int comprobarGuanyat() {
        int h = comprobarHoritzontal();
        int v = comprobarVertical();
        int d = comprobarDiagonal();
        if(h == 1 || v == 1 || d == 1)
            return 1;
        else if(h == 2 || v == 2 || d == 2)
            return 2;
        return 0;
    }

    public int comprobarHoritzontal() {
        int contador1, contador2;

        for(int y = 0; y < fitxes.length; y++) {
            contador1 = contador2 = 0;
            for(int x = 0; x < fitxes[y].length; x++) {
                if(fitxes[x][y] == 1)
                    contador1++;
                else if(fitxes[x][y] == 2)
                    contador2++;
                if(contador1 == 3)
                    return 1;
                if(contador2 == 3)
                    return 2;
            }
        }
        return 0;
    }

    public int comprobarVertical() {
        int contador1, contador2;

            for(int x = 0; x < fitxes.length; x++) {
                contador1 = contador2 = 0;
                for(int y = 0; y < fitxes[x].length; y++) {
                    if(fitxes[x][y] == 1)
                        contador1++;
                    else if(fitxes[x][y] == 2)
                        contador2++;
                    if(contador1 == 3)
                        return 1;
                    if(contador2 == 3)
                        return 2;
            }
        }
        return 0;
    }

    public int comprobarDiagonal() {
        int contador1 = 0, contador2 = 0;

        for(int xy = 0; xy < fitxes.length; xy++) {
            if(fitxes[xy][xy] == 1)
                contador1++;
            else if(fitxes[xy][xy] == 2)
                contador2++;
            if(contador1 == 3)
                return 1;
            if(contador2 == 3)
                return 2;
        }

        contador1 = 0;
        contador2 = 0;
        for(int x = fitxes.length - 1, y = 0; x >= 0 && y < fitxes.length; x--,y++) {
            if(fitxes[x][y] == 1)
                contador1++;
            else if(fitxes[x][y] == 2)
                contador2++;
            if(contador1 == 3)
                return 1;
            if(contador2 == 3)
                return 2;
        }
        return 0;
    }

    public int[] millorTirada(int fitxa) {
        llocsValids = new int[9][2];
        int index = 0;

        for(int y = 0; y < fitxes.length; y++) {
            for (int x = 0; x < fitxes[y].length; x++) {
                if(fitxes[x][y] == 0) {
                    llocsValids[index][0] = x;
                    llocsValids[index][1] = y;
                    index++;
                }
            }
        }
        if(index == 0)
            return new int[]{-1,-1};

        int[] h,v,d;
        h = millorHoritzontal(fitxa);
        if(!Arrays.equals(h,new int[]{-1,-1})) {
            for(int i = 0; i < index; i++) {
                if(Arrays.equals(llocsValids[i],h)) {
                    System.out.println(llocsValids[i][0] + "," + llocsValids[i][1] + ":" + h[0] + "," + h[1]);
                    return llocsValids[i];
                }
            }
        }
        v = millorVertical(fitxa);
        if(!Arrays.equals(v,new int[]{-1,-1})) {
            for(int i = 0; i < index; i++) {
                if(Arrays.equals(llocsValids[i],v)) {
                    System.out.println(llocsValids[i][0] + "," + llocsValids[i][1] + ":" + v[0] + "," + v[1]);
                    return llocsValids[i];
                }
            }
        }
        d = millorDiagonal(fitxa);
        if(!Arrays.equals(d,new int[]{-1,-1})) {
            for(int i = 0; i < index; i++) {
                if(Arrays.equals(llocsValids[i],d)) {
                    System.out.println(llocsValids[i][0] + "," + llocsValids[i][1] + ":" + d[0] + "," + d[1]);
                    return llocsValids[i];
                }
            }
        }
        return new int[]{-1,-1};
    }

    public int[] tiradaRandom() {
        int index = 0;

        for(int y = 0; y < fitxes.length; y++) {
            for (int x = 0; x < fitxes[y].length; x++) {
                if(fitxes[x][y] == 0) {
                    llocsValids[index][0] = x;
                    llocsValids[index][1] = y;
                    index++;
                }
            }
        }
        if(index == 0)
            return new int[]{-1,-1};
        return llocsValids[(int)(Math.random()*index)];
    }

    private int[] millorHoritzontal(int fitxa) {
        int tmpX = -1, tmpY = -1;
        int contador;

        for(int y = 0; y < fitxes.length; y++) {
            contador = 0;
            for(int x = 0; x < fitxes[y].length; x++) {
                if(fitxes[x][y] == 0) {
                    tmpX = x;
                    tmpY = y;
                }
                if(fitxes[x][y] == fitxa)
                    contador++;
            }
            if(contador == 2)
                return new int[]{tmpX,tmpY};
        }
        return new int[]{-1,-1};
    }

    private int[] millorVertical(int fitxa) {
        int tmpX = -1, tmpY = -1;
        int contador;

        for(int x = 0; x < fitxes.length; x++) {
            contador = 0;
            for(int y = 0; y < fitxes[x].length; y++) {
                if(fitxes[x][y] == 0) {
                    tmpX = x;
                    tmpY = y;
                }
                if(fitxes[x][y] == fitxa)
                    contador++;
            }
            if(contador == 2)
                return new int[]{tmpX,tmpY};
        }
        return new int[]{-1,-1};
    }

    private int[] millorDiagonal(int fitxa) {
        int contador = 0;
        int tmpX = -1, tmpY = -1;

        for(int xy = 0; xy < fitxes.length; xy++) {
            if(fitxes[xy][xy] == 0)
                tmpX = tmpY = xy;
            if(fitxes[xy][xy] == fitxa)
                contador++;
        }
        if(contador == 2)
            return new int[]{tmpX,tmpY};

        contador = 0;
        for(int x = fitxes.length - 1, y = 0; x >= 0 && y < fitxes.length; x--,y++) {
            if(fitxes[x][y] == 0) {
                tmpX = x;
                tmpY = y;
            }
            if(fitxes[x][y] == fitxa)
                contador++;
        }
        if(contador == 2)
            return new int[]{tmpX,tmpY};
        return new int[]{-1,-1};
    }

}
