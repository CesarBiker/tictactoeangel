import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Arrays;
import java.util.Random;

/*
 * Created by Àngel Mariages on 11/20/15.
 */

public class FinestraJoc {

    private JButton tauler[][];
    private JButton resetButton;
    private JLabel infoLabel;
    private JTextField idField;
    private String fitxa1 = "X";
    private String fitxa2 = "O";
    private int torn = 0;
    private int midaCasella = 80;
    private int mode = 0;
    private int idJugador = 0;

    private Tauler taulerJoc = new Tauler(3,3);

    public FinestraJoc(final String titol, final int amplada) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                JFrame finestra = new JFrame(titol);

                midaCasella = amplada / 2 / 3;
                finestra.setBounds(0,0,amplada,midaCasella * 3 + 28);
                finestra.setLocationRelativeTo(null);
                finestra.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                finestra.setResizable(false);
                finestra.setLayout(null);

                afegirComponents(finestra.getContentPane());
                taulerJoc.netejarTauler();

                finestra.setVisible(true);
            }
        });
    }

    private void afegirComponents(Container finestra) {
        tauler = new JButton[3][3];

        infoLabel = new JLabel("Torn jugador " + fitxa1);
        infoLabel.setBounds(midaCasella * 3 + 20, 10, 200, 20);
        finestra.add(infoLabel);

        resetButton = new JButton("Nova partida");
        resetButton.setBounds(midaCasella * 3 + 20, 30, 200, 30);
        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                nouJoc();
            }
        });
        finestra.add(resetButton);

        final JButton modeButton = new JButton("Mode: PvP");
        modeButton.setBounds(midaCasella * 3 + 20, 60, 200, 30);
        modeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(mode == 0) {
                    mode = 1;
                    modeButton.setText("Mode: PvE");
                    resetButton.setText("Nova partida");
                    idField.setVisible(false);
                    nouJoc();
                } else if(mode == 1) {
                    mode = 2;
                    modeButton.setText("Mode: PvP Online");
                    idField.setVisible(true);
                    resetButton.setText("Iniciar servidor");
                    nouJoc();
                } else if(mode == 2) {
                    mode = 0;
                    modeButton.setText("Mode: PvP");
                    resetButton.setText("Nova partida");
                    idField.setVisible(false);
                    nouJoc();
                }
            }
        });
        finestra.add(modeButton);

        idField = new JTextField("ID jugador 2");
        idField.setVisible(false);
        idField.setBounds(midaCasella * 3 + 20, 90, 200, 20);
        idField.setHorizontalAlignment(SwingConstants.CENTER);
        idField.setFont(new Font("Arial",Font.BOLD,14));
        idField.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent focusEvent) {
                idField.selectAll();
            }

            @Override
            public void focusLost(FocusEvent focusEvent) {

            }
        });
        idField.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {

            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {
                if(keyEvent.getKeyCode() == KeyEvent.VK_ENTER) {
                    System.out.println(idField.getText());
                }
            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        });
        finestra.add(idField);

        for(int y = 0; y < 3; y++) {
            for(int x = 0; x < 3; x++) {
                final int xF = x, yF = y;
                tauler[x][y] = new JButton("-");
                tauler[x][y].setBounds(x * midaCasella, y * midaCasella, midaCasella, midaCasella);
                tauler[x][y].setFocusable(false);
                tauler[x][y].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        if(mode == 0) {
                            if (torn == 0) {
                                if (modFitxaTauler(xF, yF, "X") == 0) {
                                    infoLabel.setText("Torn jugador " + fitxa2);
                                    torn = 1;
                                }
                            } else if (torn == 1) {
                                if (modFitxaTauler(xF, yF, "O") == 0) {
                                    infoLabel.setText("Torn jugador " + fitxa1);
                                    torn = 0;
                                }
                            }
                        } else if(mode == 1) {
                            if(torn == 0) {
                                if (modFitxaTauler(xF, yF, "X") == 0) {
                                    if(taulerJoc.comprobarGuanyat() == 0) {
                                        infoLabel.setText("Torn jugador " + fitxa2);
                                        for (JButton[] aTauler : tauler) {
                                            for (JButton anATauler : aTauler) {
                                                anATauler.setEnabled(false);
                                            }
                                        }
                                        Timer espera = new Timer(500, new ActionListener() {
                                            @Override
                                            public void actionPerformed(ActionEvent actionEvent) {
                                                //int tiradaOrd[] = tirarOrdinador();
                                                int tiradaOrd[] = taulerJoc.millorTirada(2);
                                                if(Arrays.equals(tiradaOrd,new int[]{-1,-1}))
                                                    tiradaOrd = taulerJoc.tiradaRandom();
                                                modFitxaTauler(tiradaOrd[0], tiradaOrd[1], "O");
                                                infoLabel.setText("Torn jugador " + fitxa1);
                                                for (JButton[] aTauler : tauler) {
                                                    for (JButton anATauler : aTauler) {
                                                        anATauler.setEnabled(true);
                                                    }
                                                }
                                                comprobarGuanyat();
                                            }
                                        });
                                        espera.setRepeats(false);
                                        espera.start();
                                    }
                                }
                            }
                        }
                    }
                });
                finestra.add(tauler[x][y]);
            }
        }
    }

    public void comprobarGuanyat() {
        if(taulerJoc.comprobarGuanyat() == 1)
            jocGuanyat(1);
        else if(taulerJoc.comprobarGuanyat() == 2)
            jocGuanyat(2);
    }

    private void jocGuanyat(int i) {
        if(i == 1)
            infoLabel.setText("El jugador " + fitxa1 + " ha guanyat!");
        if(i == 2)
            infoLabel.setText("El jugador " + fitxa2 + " ha guanyat!");
        for (JButton[] aTauler : tauler) {
            for (JButton anATauler : aTauler) {
                anATauler.setEnabled(false);
            }
        }
    }

    private void nouJoc() {
        if(mode == 0 || mode == 1) {
            taulerJoc.netejarTauler();

            for (JButton[] aTauler : tauler) {
                for (JButton anATauler : aTauler) {
                    torn = 0;
                    infoLabel.setText("Torn jugador " + fitxa1);
                    anATauler.setEnabled(true);
                    anATauler.setText("-");
                }
            }

            Random rnd = new Random();
            if (rnd.nextInt(2) == 1 && mode == 1) {
                comprobarGuanyat();
                infoLabel.setText("Torn jugador " + fitxa2);
                for (JButton[] aTauler : tauler) {
                    for (JButton anATauler : aTauler) {
                        anATauler.setEnabled(false);
                    }
                }
                Timer espera = new Timer(500, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        //int tiradaOrd[] = tirarOrdinador();
                        int tiradaOrd[] = taulerJoc.millorTirada(2);
                        if (Arrays.equals(tiradaOrd, new int[]{-1, -1}))
                            tiradaOrd = taulerJoc.tiradaRandom();
                        //System.out.println("Tiradax:"+tiradaOrd[0]+","+tiradaOrd[1]);
                        modFitxaTauler(tiradaOrd[0], tiradaOrd[1], "O");
                        infoLabel.setText("Torn jugador " + fitxa1);
                        for (JButton[] aTauler : tauler) {
                            for (JButton anATauler : aTauler) {
                                anATauler.setEnabled(true);
                            }
                        }

                        comprobarGuanyat();
                    }
                });
                espera.setRepeats(false);
                espera.start();
            }
        } else if(mode == 2) {

        }
    }

    private int modFitxaTauler(int x, int y, String fitxa) {
        int cF = -1;
        if(fitxa.equals("X")) {
            cF = taulerJoc.canviarFitxa(x,y,1);
            if(cF == 0) {
                tauler[x][y].setText("X");
                comprobarGuanyat();
            }
            return cF;
        } else if(fitxa.equals("O")) {
            cF = taulerJoc.canviarFitxa(x,y,2);
            if(cF == 0) {
                tauler[x][y].setText("O");
                comprobarGuanyat();
            }
            return cF;
        }
        return cF;
    }
}
