import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/*
 * Created by Àngel Mariages on 11/30/15
 */
public class ConsultaWeb extends Thread {

    private boolean multiLinia;
    private String params = "";
    private String url;
    public String resposta = "";

    public void run() {
        resposta = "";
        try {
            long inici = System.nanoTime();
            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());

            writer.write(params);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            if(multiLinia) {
                String linia;
                while((linia = reader.readLine()) != null) {
                    resposta += "\n" + linia;
                }
            } else {
                resposta = reader.readLine();
            }

            double fi = (System.nanoTime() - inici) / 1000000000.;
            System.out.printf("ConsultaWeb->Executada en: %.3fs\n",fi);

            reader.close();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ConsultaWeb(String url, boolean multiLinia) {
        this.url = url;
        this.multiLinia = multiLinia;
    }

    public void afegirParametres(String parametre) {
        this.params += parametre;
    }

    public void modificarParametres(String parametres) {
        this.params = parametres;
    }
}
