/*
 * Created by Àngel Mariages on 11/20/15.
 */
public class JocPrincipal {
    public static void main(String[] args) {
        //new FinestraJoc("TicTacAngel", 480);
        //new FinestraJoc("TicTacAngel", 480);

        ConsultaWeb consultaWeb = new ConsultaWeb("http://localhost/enratlla.php", false);
        consultaWeb.modificarParametres("activeID=51");
        consultaWeb.run();

        System.out.println(consultaWeb.resposta);

        consultaWeb.modificarParametres("activeID=50");
        consultaWeb.run();

        System.out.println(consultaWeb.resposta);

        consultaWeb.modificarParametres("newID=50");
        consultaWeb.run();

        System.out.println(consultaWeb.resposta);

        consultaWeb.modificarParametres("newID=51");
        consultaWeb.run();

        System.out.println(consultaWeb.resposta);
    }
}
